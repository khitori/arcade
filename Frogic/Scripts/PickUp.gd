extends Area2D

#Le nom del'objet a ramasser, pour ajouter dans l'inventair
#Comme cet variable utilise Export, on peut la modifier sur la node elle meme
export var nom = "cercle"

var ajouter = false

func _ready():
	#Lance l'animation de base
	get_node("AnimationPlayer").play("Idle")

func _on_body_entered(body):
	#Regarde si cet objet a deja ete ramasser
	#Ceci empeche de l'ajouter 2 fois si les joueurs interagisse en meme temps
	if ajouter == false:
		ajouter = true
		
		#Dis au body de ramasser cet objet
		body.ramasser_objet(self)
		
		#Detruit l'objet un coup qu'il est libre
		queue_free()
