extends CanvasLayer
var temps = 0.0

func _ready():
	GameManager.ui = self
	GameManager.can_move = false
	get_node("Transition").visible = true
	get_node("Transition/Label").text = "Niveau " + str(GameManager.niveau)
	get_node("Transition/AnimationPlayer").play("TRANSIT_IN")
	yield(get_tree().create_timer(1.5),"timeout")
	GameManager.can_move = true

func _process(delta):
	if GameManager.can_move == true:
		temps += delta

	var format_string = "%0*.*f"

	get_node("Inv/Temps").text = (format_string % [6, 2, temps])
	
func update_ui(joueur_un,joueur_deux):
	get_node("Inv/P1-Inventaire/Cercle/Label").text = str(joueur_un["cercle"])
	get_node("Inv/P1-Inventaire/Losange/Label").text = str(joueur_un["losange"])
	get_node("Inv/P1-Inventaire/X/Label").text = str(joueur_un["x"])
	
	get_node("Inv/P2-Inventaire/Cercle/Label").text = str(joueur_deux["cercle"])
	get_node("Inv/P2-Inventaire/Losange/Label").text = str(joueur_deux["losange"])
	get_node("Inv/P2-Inventaire/X/Label").text = str(joueur_deux["x"])

func gele():
	get_node("Glace").visible = true
	get_node("Glace/AnimationPlayer").play("Idle")
	
func degele():
	get_node("Glace").visible = false

func utiliser_pouvoir(_pouvoir):
	get_node("Power/Label").text = _pouvoir 
	get_node("Power/AnimationPlayer").play("USE_POWER")
	
func transit_out(_t):

	get_node("Transition/Label").text = _t


	get_node("Transition/AnimationPlayer").play("TRANSIT_OUT")
