extends KinematicBody2D
#Different etat de notre joueur
enum States {Idle,Course,Pencher,Saut}

const VITESSE_DEPLACEMENT = 80
const GRAVITE = 12
const FORCE_SAUT = 175

#Valeur pour abandonner niveau
var abandon = false

#Nos 2 differentes collisions
onready var collision_normal = get_node("CollisionShape2D")
onready var collision_pencher = get_node("Pencher/CollisionShape2D")


var direction = Vector2(0,0)
var state = States.Idle
var animation_player
var sprite
var audio

var saut = 2
var timer_saut
export var J = "P1"

#Quand le jeu commence
func _ready():
	#Va chercher nos nodes en reference
	animation_player = get_node("AnimationPlayer")
	sprite = get_node("Sprite")
	audio = get_node("Audio")
	timer_saut = get_node("JumpForgiveness")

#A chaque frame, environ 60 fois par seconde
func _process(delta):
	#Si on ne peut pas bouger
	if GameManager.can_move == false:
		#Arrete la fonction ici
		return
	
	#Si on joue en mode solo
	if GameManager.solo == true:
		#Si on controlle le joueur 1 et ceci est le joueur 1 ou on controlle le joueur 2 et ceci est le joueur 2
		if (GameManager.controlling_player_one == true && J == "P1") || (GameManager.controlling_player_one == false && J == "P2"):
			
			#Si on appuie sur start
			if Input.is_action_just_pressed("P1-START"):
				
				#Attend 1 dixieme de seconde
				yield(get_tree().create_timer(0.1), "timeout")
				
				#Change quel joueur on control
				GameManager.controlling_player_one = !GameManager.controlling_player_one
			
			#Regarde si un des boutons suivant est enfoncer, utilise le pouvoir associer
			if Input.is_action_just_pressed("P1-A"): 
				p_champignon() 
			if Input.is_action_just_pressed("P1-X"):
				p_teleport()
			if Input.is_action_just_pressed("P1-Y"):
				p_gele()
	else:
		#Regarde si un des boutons suivant est enfoncer, utilise le pouvoir associer
		#Cet fois on ajoute J pour regarder l'input corespondant au joueur 1 ou joueur 2
		if Input.is_action_just_pressed(J+"-A"): 
			p_champignon() 
		if Input.is_action_just_pressed(J+"-X"):
			p_teleport()
		if Input.is_action_just_pressed(J+"-Y"):
			p_gele()
	
	#Regarde si on abandonne la partie
	verifier_abandon_de_partie()


func verifier_abandon_de_partie():
	#Si on est le joueur 1
	if J == "P1":
		#et on appuie sur start
		if Input.is_action_just_pressed("P1-START"):
			#Si abandon est vrai
			if abandon == true:
				#Dis a l'eau que ce joueur lui a toucher (perd la partie)
				GameManager.eau._on_Eau_body_entered(self)
			#Met abandon a vrai
			abandon = true

#A toute les physics update (environ 60 fois par seconde)
func _physics_process(delta):
	
	#Bouge notre joueur et garde son deplacement
	var velocite = mouvement()
	
	#Anime notre joueur en fonction de son deplacement
	animation(velocite)

func mouvement():
	#remet notre deplacement horizontal a 0
	direction.x = 0
	
	#Si on peu bouger
	if GameManager.can_move == true:
		#Si on joue seul, utilise le mouvement solo
		if GameManager.solo == true:
			single_player_movement()
		
		#Sinon utilise le mouvement a deux
		else:
			two_player_movement()
	else:
		#si on ne peut pas bouger, applique la graviter quand meme
		direction.y = clamp(direction.y + GRAVITE,-400,400)
	
	#Si on est presentement pencher, 
	if state == States.Pencher:
		return Vector2(0,0)
		
	if is_on_floor() == false:
		direction.x *= 1.25
		state = States.Saut
		audio.arreter_marcher()
	else:
		if direction.x != 0 :
			state = States.Course
		else:
			state = States.Idle
				

	var velocite = move_and_slide(Vector2(direction.x * VITESSE_DEPLACEMENT,direction.y),Vector2.UP)
	
	return velocite
	
func single_player_movement():
	if (GameManager.controlling_player_one == true && J == "P1") || (GameManager.controlling_player_one == false && J == "P2"):
		if Input.is_action_pressed("P1-DROITE"):
			direction.x += 1
		if Input.is_action_pressed("P1-GAUCHE"):
			direction.x -= 1
					
		if Input.is_action_just_released("P1-BAS"):
			collision_normal.disabled = false
			collision_pencher.disabled = true
			state = States.Idle
						
		if is_on_floor() == true:
			if Input.is_action_just_pressed("P1-BAS"):
				collision_normal.disabled = true
				collision_pencher.disabled = false
				state = States.Pencher
						
			if Input.is_action_pressed("P1-BAS"):
				direction.x = 0
				direction.y = 0
						
			if Input.is_action_just_pressed("P1-SAUT"):
				audio.jouer_son("Saut1",true)
				direction.y = -FORCE_SAUT
			else:
				direction.y = 1
		else:
			if saut > 0:
				if Input.is_action_just_pressed("P1-SAUT"):
					audio.jouer_son("Saut2",true)
					direction.y = (-FORCE_SAUT * 1.3) + 5
			if collision_normal.disabled == false:
				direction.y = clamp(direction.y + GRAVITE,-400,400)
	
func two_player_movement():
	if Input.is_action_pressed(J+"-DROITE"):
		direction.x += 1
	if Input.is_action_pressed(J+"-GAUCHE"):
		direction.x -= 1

	if Input.is_action_just_released(J+"-BAS"):
		collision_normal.disabled = false
		collision_pencher.disabled = true
		state = States.Idle
	
	if is_on_floor() == true:
		if Input.is_action_just_pressed(J+"-BAS"):
			collision_normal.disabled = true
			collision_pencher.disabled = false
			state = States.Pencher
						
		if Input.is_action_pressed(J+"-BAS"):
			direction.x = 0
			direction.y = 0
			
		if get_node("JumpForgiveness").is_stopped() == false:
			get_node("JumpForgiveness").stop()
			
		saut = 2
		if Input.is_action_just_pressed(J+"-SAUT"):
			audio.jouer_son("Saut1",true)
			direction.y = -FORCE_SAUT
			saut -= 1
		else:
			direction.y = 1
	else:
		if saut == 2 and get_node("JumpForgiveness").is_stopped() == true: 
			get_node("JumpForgiveness").start()
		if saut > 0:
			if Input.is_action_just_pressed(J+"-SAUT"):
				audio.jouer_son("Saut2",true)
				saut -= 1
				direction.y = (-FORCE_SAUT * 1.3) + 5
		if collision_normal.disabled == false:
			direction.y = clamp(direction.y + GRAVITE,-400,400)
	
func animation(_velocite):
	if _velocite.x > .1 :
		sprite.flip_h = false
	elif _velocite.x < -.1:
		sprite.flip_h = true
		
	if state == States.Saut and animation_player.current_animation != "Saut":
		animation_player.play("Saut")
	elif state == States.Course and animation_player.current_animation != "Course":
		audio.jouer_son("Marcher",true)
		animation_player.play("Course")
	elif state == States.Pencher and animation_player.current_animation != "Pencher":
		animation_player.play("Pencher")
	elif state == States.Idle and animation_player.current_animation != "Idle":
		animation_player.play("Inactif")
		audio.arreter_marcher()

func ramasser_objet(objet):
	audio.jouer_son("Ramasser")
	if J == "P1":
		if GameManager.inventaire_joueur_un.has(objet.nom):
			GameManager.inventaire_joueur_un[objet.nom] +=1
			GameManager.update_ui()
	else:
		if GameManager.inventaire_joueur_deux.has(objet.nom):
			GameManager.inventaire_joueur_deux[objet.nom] += 1
			GameManager.update_ui()

func p_champignon():
	if is_on_floor():
		if utiliser_objet("x"):
			audio.jouer_son("Champignon")
			GameManager.ui.utiliser_pouvoir("Champignon!")
			
			var res_champignon = load("res://Frogic/Scenes/Champignon.tscn")
			var instance = res_champignon.instance()
			get_parent().add_child(instance)
			
			instance.position = position
	
func p_teleport():
	if utiliser_objet("cercle"):
		audio.jouer_son("Teleportation")
		GameManager.ui.utiliser_pouvoir("Teleportation!")
		
		var joueur_cible
		
		for n in get_tree().get_nodes_in_group("JOUEUR"):
			if n != self:
				joueur_cible = n
				
		joueur_cible.position = position
		joueur_cible.state = States.Idle
		joueur_cible.collision_normal.disabled = false
		joueur_cible.collision_pencher.disabled = true

func p_gele():
	if utiliser_objet("losange"):
		audio.jouer_son("Glacer")
		GameManager.ui.utiliser_pouvoir("Glace!")
		
		GameManager.eau.gele(5)
		
func utiliser_objet(_objet):
	var inventaire
	# Choisie inventaire
	if J == "P1" :
		inventaire = GameManager.inventaire_joueur_un
	else:
		inventaire = GameManager.inventaire_joueur_deux
	#on regarde si on a l'objet designer
	if inventaire[_objet] > 0:
		inventaire[_objet] -= 1
		GameManager.update_ui()
		return true
		#si oui on l'utilise
	else:
		return false
		#si non on l'utilise pas


func _on_JumpForgiveness_timeout():
	if is_on_floor() == false and saut >= 2:
		saut = 1
