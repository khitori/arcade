extends Node2D
#Gere les sons des joueurs

#Dictionnaire d'effet sonore
var sfx

#Quand la partie commence
func _ready():
	creer_dictionnaire()

func creer_dictionnaire():
	#Nous allons chercher toute les nodes de sons suivante
	sfx = {
	"Marcher" : get_node("Marche"),
	"Saut1" : get_node("Saut1"),
	"Saut2" : get_node("Saut2"),
	"Tomber" : get_node("Tomber"),
	"Ramasser" : get_node("Ramasser"),
	"Glacer" : get_node("Glacer"),
	"Champignon" : get_node("Champignon"),
	"Teleportation" : get_node("Teleportation")
	}
	
#Jouer le son specifier dans le parametre m
func jouer_son(son,m = false):
	#Alterne un peu le pitch, pour empecher la monotoniter
	sfx[son].pitch_scale = rand_range(0.95,1.05)
	
	#Arrete le son s'il est deja en cours
	sfx[son].stop()
	
	#Joue le son a partir de 0 seconde
	sfx[son].play(0)
	
#Aretter le son de marche, puisque celui-ci est fait pour looper
func arreter_marcher():
	sfx["Marcher"].stop()

