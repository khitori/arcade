extends Area2D

var frogs = 0

func _ready():
	get_node("AnimationPlayer").play("Idle")

func _on_Zone_Victoire_body_entered(body):
	frogs += 1
	
	if frogs >= 2 :
		win()

func _on_Zone_Victoire_body_exited(body):
	frogs -= 1

func win():
	Analytics._finished_level(GameManager.niveau)
	Analytics._add_playtime(GameManager.ui.temps)
	GameManager.can_move = false
	GameManager.eau.VITESSE = 0
	yield(get_tree().create_timer(1.5), "timeout")
	
	GameManager.ui.transit_out("Niveau terminer\n\nTemps : " + str(GameManager.ui.temps))
	
	yield(get_tree().create_timer(2.0), "timeout")

	GameManager.inventaire_a_zero()
		
	GameManager.update_ui()
	
	if GameManager.arcade == true:
		get_tree().change_scene("res://Frogic/Scenes/A-Level Select.tscn")
	else:
		get_tree().change_scene("res://Frogic/Scenes/O-Level Select.tscn")

