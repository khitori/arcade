extends AudioStreamPlayer

#Variable qui represente une liste de chanson
var songs = []

func _ready():
	#Change la seed pour avoir differente valeur a chaque fois que le jeu demarre
	randomize()
	
	#Ajoute les chansons a la liste
	songs.append(load("res://Frogic/Audio/Songs/8BitCave.wav"))
	songs.append(load("res://Frogic/Audio/Songs/napping_on_a_cloud.ogg"))
	songs.append(load("res://Frogic/Audio/Songs/time to puzzle_Master.ogg"))
	#songs.append(load("res://Frogic/Audio/Songs/wyver9_Fast Level.wav"))
	songs.append(load("res://Frogic/Audio/Songs/wyver9_Funny Chase (8-bit).wav"))
	songs.append(load("res://Frogic/Audio/Songs/Australis Frontier.wav"))
	
	#Connecte le signal "finished" du streamplayer a la fonction song_ended
	connect("finished",self,"song_ended")

	play_new_song()

func song_ended():
	play_new_song()

func play_new_song():
	#Choisi une chanson au hasard et assigne au stream player
	set_stream(songs[randi()%songs.size()])
	
	#Fait jouer la chanson actuel
	play()
