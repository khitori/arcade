extends Panel



func _on_Change_pressed():
	if get_node("Controller").visible == true:
		get_node("Controller").visible = false
		get_node("Keyboard").visible = true
		get_node("Buttons/Change").text = "Controller"
	else:
		get_node("Controller").visible = true
		get_node("Keyboard").visible = false
		get_node("Buttons/Change").text = "Keyboard"


func _on_Close_pressed():
	self.visible = false


func _on_Controls_pressed():
	self.visible = true
