extends Control

#Volume du jeu
var volume = 20
#est-ce que le joueur control la bar de son
var son_focus = false
#grosseur original du slider
var original_size

var niveau_1
var son_highlight

#Quand le jeux commence
func _ready():
	print(get_tree().current_scene.name)
	if GameManager.arcade == true and get_tree().current_scene.name != "A-Level Select":
		get_tree().change_scene("res://Frogic/Scenes/A-Level Select.tscn")
	elif GameManager.arcade == false and get_tree().current_scene.name != "O-Level Select":
		get_tree().change_scene("res://Frogic/Scenes/O-Level Select.tscn")
	
	#Va chercher la grosseur initial du slider
	original_size = get_node("Son/Panel").rect_size.x
	
	niveau_1 = get_node("Title/GridContainer/1")
	son_highlight = get_node("Son/Highlight")
	
	#Donne le focus (le curseur) sa position, pour naviguer avec la manette
	if GameManager.arcade == true:
		niveau_1.grab_focus()

#Fonction pour commencer un niveau
func _change_level(_level):
	#Assigne le niveau au game manager
	GameManager.niveau = _level
	
	#Ajoute les donner dans l'analytic et sauvegarde
	Analytics._started_level(_level)
	Analytics.save_data()
	
	#fais sure que nos inventaire son a zero quand la partie commence
	GameManager.inventaire_a_zero()
	
	#remet nos essaie a 3
	GameManager.essaie = 3
	
	#Change la scene pour le niveau specifier
	#Ici nous assemblons notre chemin avec le parametre _level specifier
	get_tree().change_scene("res://Frogic/Scenes/Niveaux/Niveau "+str(_level)+".tscn")

#A chaque frame (60 fois par seconde)
func _process(delta):
	#Si le joueur 1 appuie sur haut ou bas
	if Input.is_action_just_pressed("P1-BAS") or Input.is_action_just_pressed("P1-HAUT") :
		#Si on control les niveaux
		if son_focus == false:
			
			#on control le son
			son_focus = true
			
			#donne le focus on son (pour elenver le focus des niveaux)
			get_node("Son/Focus").grab_focus()
			
			#Affiche la bordure blanche du son
			son_highlight.visible = true
			
		#si on control le son
		else:
			#on ne control pas le son
			son_focus = false
			
			#Donne le focus au niveaux
			niveau_1.grab_focus()
			
			#Cache la bordure blanche du son
			son_highlight.visible = false
			
	#Si on control le son
	if son_focus == true:
		#Si on appuie sur droite
		if Input.is_action_just_pressed("P1-DROITE"):
			#Si le volume est plus bas que 20
			if volume < 20:
				volume += 1
				changer_volume()
				
		#Si on appui sur gauche
		elif Input.is_action_just_pressed("P1-GAUCHE"):
			#Si le volume est plus que 0
			if volume > 0 :
				volume -= 1
				changer_volume()

func changer_volume():
	#Ajuste le volume du master bus sur une base de 80 decibel
	AudioServer.set_bus_volume_db(0,-80 + volume * 4)
	#Ajuste la largeur de notre slider en relation au volume
	get_node("Son/Panel").margin_right = -2 - (20- volume) * (original_size/20)
	#Fait jouer un son comme reference
	get_node("AudioStreamPlayer2D").play()


func _on_SoundUp_pressed():
	if volume < 20:
		volume += 1
		changer_volume()


func _on_SoundDown_pressed():
	if volume > 0 :
		volume -= 1
		changer_volume()
