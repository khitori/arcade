extends Node2D

export var VITESSE = 3

var temps_gele = 0
var perdu = false

var glace
var shape_glace

#Quand le jeu commence
func _ready():
	#Associe cet node a la variabl eau du gamemanager
	GameManager.eau = self
	
	#Assigne nos 2 variable
	glace = get_node("Glace")
	shape_glace = get_node("Glace/CollisionShape2D")

#A chaque frame (60 fois par seconde)
func _process(delta):
	#Si notre temps de gele est moins de 0
	if temps_gele <= 0:
		# et notre glace est visible, lancer la fonction degele
		if glace.visible == true:
			degele()
		
		#deplacer l'eau (la faire monter)
		global_position.y -= VITESSE * delta
	else:
		#si temps de gele est plus grand que 0
		#enlever delta (le temps en seconde de la frame)
		temps_gele -= delta


func gele(_duree):
	#Variable temps de gele = le temps specifier au parametre _duree
	temps_gele = _duree
	
	#Affiche la glace a l'ecran
	glace.visible = true
	
	#Rend la zone de glace active (pour marcher dessu)
	shape_glace.disabled = false
	
	#Accede au ui via le gamemanager
	#et lance l'effet de gele
	GameManager.ui.gele()

func degele():
	#Cache la glace
	glace.visible = false
	
	#Desactive la zone de glace (pour ne plus marcher dessu)
	shape_glace.disabled = true

	#Accede au ui via le gamemanager
	#et lance l'effet de degele
	GameManager.ui.degele()

#Quand un body entre dans l'eau (un joueur)
func _on_Eau_body_entered(body):
	#Accede a l'analyse et ajoute le temps de jeu
	Analytics._add_playtime(GameManager.ui.temps)
	
	#Si la demarche de perte n'est pas commencer
	if perdu == false:
		
		#Commence la demarche de perte
		perdu = true
		
		#Accede au game manager et enleve 1 essaie
		GameManager.essaie -= 1
		
		#S'il reste des essaie
		if GameManager.essaie > 0:
			#Accede au UI via le gamemanager
			#Lance le changement de niveau avec text specifier
			GameManager.ui.transit_out("Grenouille a la mer !\nEssaie restant : " + str(GameManager.essaie))
			
			#Attrape la node Vie, et joue son son.
			get_node("Vie").play()
			
			#Attend 2 seconde, le temp que l'animation joue
			yield(get_tree().create_timer(2.0), "timeout")
			
			#Remet l'inventaire des joueurs a zero
			GameManager.inventaire_a_zero()
		
			#Met le ui a jour
			GameManager.update_ui()
			
			#Relance la scene actuel (rejouer le niveau)
			get_tree().reload_current_scene()
			
		#Sinon, si nous n'avons plus d'essaie
		else:
			#Accede au UI via le gamemanager
			#Lance le changement de niveau avec text specifier
			GameManager.ui.transit_out("Grenouille a la mer !\nNext!")
			
			#Attrape la node Next et joue son son
			get_node("Next").play()
			
			#Attend 2 seconde, le temp que l'animation joue
			yield(get_tree().create_timer(2.0), "timeout")
			
			#Remet l'inventaire des joueurs a zero
			GameManager.inventaire_a_zero()
		
			#Met le ui a jour
			GameManager.update_ui()
		
			#Change la scene actuel pour retourner au menu principal
			get_tree().change_scene("res://Spice&Thrice/Scenes/Level Select.tscn")
	
