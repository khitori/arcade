extends Node

var file_path = "user://save-file.save"
var data = {
	"level_1_started":0,
	"level_2_started":0,
	"level_3_started":0,
	"level_4_started":0,
	"level_5_started" :0,
	"level_1_finished":0,
	"level_2_finished":0,
	"level_3_finished":0,
	"level_4_finished":0,
	"level_5_finished" : 0,
	"total_playtime":0,
	"level_1_fastest":0,
	"level_2_fastest":0,
	"level_3_fastest":0,
	"level_4_fastest":0,
	"level_5_fastest":0
}

func _ready():
	load_data()
	
func save_data():
	var file = File.new()
	file.open(file_path,file.WRITE)
	
	file.store_var(to_json(data),false)
	file.close()

func load_data():
	var file = File.new()
	if(file.file_exists(file_path)):
		file.open(file_path,File.READ)
		
		data = parse_json(file.get_var(false))
		file.close()

func _started_level(_level):
	data["level_"+str(_level)+"_started"] += 1
	save_data()
	
func _finished_level(_level):
	data["level_"+str(_level)+"_finished"] += 1
	save_data()
	
func _add_playtime(_time):
	data["total_playtime"] += _time/60
	save_data()
