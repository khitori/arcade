extends Node
#Dis si on joue seul et si oui, quel joueur on joue actuellement
var solo = false
var controlling_player_one = true
var arcade = false

#Reference au UI
var ui
#Referenec a l'eau
var eau

#Nombre d'essaie restant
var essaie = 3
#si les joueurs peuvent bouger
var can_move = false
#Niveau actuel
var niveau = 1

#Nos 2 inventaire, storer dans des dictionnaire
var inventaire_joueur_un = {
	"losange" : 0,
	"cercle" : 0,
	"x" : 0
}

var inventaire_joueur_deux = {
	"losange" : 0,
	"cercle" : 0,
	"x" : 0
}

#Fonction pour remettre les inventaire a leur valeur initial
func inventaire_a_zero():
	inventaire_joueur_un = {
	"losange" : 0,
	"cercle" : 0,
	"x" : 0
}

	inventaire_joueur_deux = {
	"losange" : 0,
	"cercle" : 0,
	"x" : 0
}

#Dis au ui de ce mettre a jour avec les 2 inventaires
func update_ui():
	ui.update_ui(inventaire_joueur_un,inventaire_joueur_deux)
